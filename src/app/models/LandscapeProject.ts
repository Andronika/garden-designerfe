import { GardenStructure } from './GardenStructure';
import { CoordinatesXY } from 'src/app/models/CoordinatesXY';

export interface LandscapeProject{
    structure: GardenStructure;
    amount: number;
    Areas: CoordinatesXY[];
}