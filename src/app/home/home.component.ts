import { Component, OnInit } from '@angular/core';
import { CoordinatesXY } from '../models/CoordinatesXY';
import { SharedDataService } from '../services/shared-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  axes: CoordinatesXY = {AreaX: null, AreaY: null};

  constructor(private sharedDataService: SharedDataService,
              private router: Router) { }

  ngOnInit() {
    this.sharedDataService.definedSize.subscribe(size => this.axes = size);
  }

 defineSize(){
   this.sharedDataService.defineSize(this.axes);
   this.router.navigateByUrl('/designer');
 }
}
