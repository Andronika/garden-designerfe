import { Component, OnInit } from '@angular/core';
import { GardenStructure } from '../models/GardenStructure';
import { GardenStructuresService } from '../services/garden-structures.service';
import { LandscapeProject } from '../models/LandscapeProject';
import { CoordinatesXY } from '../models/CoordinatesXY';
import { SharedDataService } from '../services/shared-data.service';
import { areAllEquivalent } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'designer',
  templateUrl: './designer.component.html',
  styleUrls: ['./designer.component.css']
})
export class DesignerComponent implements OnInit {
  
  structureTypes: string[];
  sizeX: number[] = [];
  sizeY: number[] = [];
  landscapeProject: LandscapeProject[];

  colors = [{ name: 'green', value: '#7dcea0'}, 
            { name: 'dark-green', value: '#307330'}, 
            { name: 'neon-green', value: '#28f728'}, 
            { name: 'grey',value: '#baccba'}];

  selected= { name: 'green',value: '#7dcea0'}
  
  constructor(private gardenStructureService: GardenStructuresService,
              private sharedDataService: SharedDataService) { }

  ngOnInit() {
    this.fillStructureTypes();
    this.fillStructures();
    this.createCrissCross();
  }

  fillStructureTypes() {
    this.gardenStructureService.getGardenStructuresTypes().subscribe((types : string[]) => {
      this.structureTypes=types;
    });
  }

  fillStructures() {
     this.gardenStructureService.getGardenStructures().subscribe((structures : GardenStructure[]) => {
       this.landscapeProject = [];
       structures.forEach(structure => {
         this.landscapeProject.push(<LandscapeProject> { structure: structure, 
                                                         amount: 0, 
                                                         Areas: [ {AreaX: null, AreaY: null} as CoordinatesXY]});
       });
     });
  }

  createCrissCross(){
    this.sharedDataService.definedSize.subscribe(size => {
      this.sizeX = this.convertSizeToArray(size.AreaX);
      this.sizeY = this.convertSizeToArray(size.AreaY);
    });
  }

  convertSizeToArray (size: number): number[] {
    let array: number[] = [];

    for(let i =0; i < size; i++){
      array.push(i);
    }
    return array;
  }

  allowDrop(ev) {
    ev.preventDefault();
  }
  
  drag(ev) {
    ev.dataTransfer.setData("${ev.target.id}", ev.target.id);
  }
  
  drop(ev) {
    ev.preventDefault();

    let structure: string = ev.dataTransfer.getData("${ev.target.id}");
    var copiedStructure = document.getElementById(structure).cloneNode(true);
    ev.target.appendChild(copiedStructure);
    ev.stopPropagation();

    this.addStructureDetailsToProject(ev, structure);
  }

  addStructureDetailsToProject(ev: any, structure: string){

    var plant = this.landscapeProject.find(p => structure.toLowerCase().
                                                      includes(p.structure.Name.toLowerCase()));
    plant.amount = plant.amount + 1;

    let targetID: string = ev.target.id;
    var coordinate = targetID.match(/\d+/g).map(Number);
    plant.Areas.push({AreaX: coordinate[0], AreaY: coordinate[1]} as CoordinatesXY);
  }

  saveProject(){
    let simplifiedProject = this.landscapeProject.filter(s => s.amount != 0);
    simplifiedProject.forEach(structure => {
      structure.Areas = structure.Areas.filter(a => a.AreaX != null);
    });
    this.gardenStructureService.createProject(simplifiedProject);
  }
}
