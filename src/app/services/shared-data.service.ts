import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CoordinatesXY } from '../models/CoordinatesXY';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  private areaSize = new BehaviorSubject<CoordinatesXY>({AreaX:10, AreaY:10});
  definedSize = this.areaSize.asObservable();

  constructor() { }

  defineSize(size: CoordinatesXY){
    this.areaSize.next(size);
  }

}
