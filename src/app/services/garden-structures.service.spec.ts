import { TestBed } from '@angular/core/testing';

import { GardenStructuresService } from './garden-structures.service';

describe('GardenStructuresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GardenStructuresService = TestBed.get(GardenStructuresService);
    expect(service).toBeTruthy();
  });
});
