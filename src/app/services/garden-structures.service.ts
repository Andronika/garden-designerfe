import { Injectable } from '@angular/core';
import { GardenStructure } from '../models/GardenStructure';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { LandscapeProject } from '../models/LandscapeProject';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GardenStructuresService {

  constructor(private http:HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  baseUrl: string = "http://localhost:63813/api";

  getGardenStructures(): Observable<GardenStructure[]>{
    return this.http.get<GardenStructure[]>(this.baseUrl + "/structures");
  }
  getGardenStructuresTypes(): Observable<string[]>{
    return this.http.get<string[]>(this.baseUrl + "/structures/types");
  }
  createProject(project: LandscapeProject[]) {
      return this.http.post(this.baseUrl + "/project", project, this.httpOptions).subscribe(r=> {});
  }
}
